# Set the base image to node:12-alpine
FROM node:16-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# # Prepare the container for building React
# RUN yarn install
# RUN yarn global add react-scripts@4.0.3
# # We want the production version
# RUN yarn build

# Prepare nginx
FROM nginx:1.19.10-alpine as release

COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
